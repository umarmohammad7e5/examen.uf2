
def letterCounter(text):
    count = 0
    for letter in list(text):
        if letter.isalpha():
            count += 1
    return count


print(letterCounter("Pol, Moreno"))
